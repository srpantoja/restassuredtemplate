# Rest Assured Template

## Descrição

Este é um projeto base para automação de testes de API utilizando RestAssured.

## Pré-requisitos

- Java JDK 21
- Maven

## Instalação

1. Clone este repositório: `git clone https://gitlab.com/srpantoja/restassuredtemplate`
2. Navegue até o diretório do projeto: `cd restassuredtemplate`
3. Compile o projeto: `mvn clean install`

## Configuração

Certifique-se de que as seguintes dependências estão instaladas:

- Java 21
- Maven
- [Allure](https://repo.maven.apache.org/maven2/io/qameta/allure/allure-commandline) (Versão 2.25.0)

## Execução dos Testes

Para executar os testes, utilize o seguinte comando:

```shell
mvn test #Para rodar todos os testes
mvn test -Dgroups=health #Para rodar os testes health
mvn test -Dgroups=contract #Para rodar os testes de contrato
mvn test -Dgroups=smoke #Para rodar os testes de fumaça
mvn test -Dgroups=regression #Para rodar os testes regressivos
```

## Principais Dependências

- RestAssured 5.4.0
- JUnit Jupiter 5.10.1
- DataFaker 2.0.2
- Allure Report 2.25.0

# Configuração do GitLab CI/CD

Este arquivo `.gitlab-ci.yml` define a pipeline de integração e
entrega contínua (CI/CD) para este projeto.
A pipeline é dividida em estágios (`stages`) que representam diferentes etapas do processo de construção, teste e
relatório.  
Além disso, é necessário criar uma variável de ambiente no gitlab com nome `BASE_URL` e remover a opção protegida, para que possa ser executada em qualquer branch.

## Estágios

### 1. Build

Este estágio compila o código-fonte do projeto usando o Maven e armazena a pasta target como artefato.

### 2. Tests

1. **Health**: Neste estágio, os testes de saúde são executados, visando verificar a saúde básica do sistema ou dos serviços.  
2. **Contract**: Os testes de contrato são realizados para garantir que as APIs sigam os contratos definidos.  
3. **Smoke**: Testes de smoke são executados para validar as funcionalidades principais do sistema.  
4. **Regression**: Este estágio executa testes de regressão para garantir que as alterações recentes não tenham introduzido regressões no sistema.

### 3. Report

Neste estágio, os relatórios do Allure são gerados a partir dos resultados dos testes. Isso inclui a geração de
relatórios HTML e arquivos de ambiente e executor.


```yaml
script:
  - chmod +x ./allure-cli/bin/allure  # Concede permissão de execução ao arquivo `allure` localizado no diretório `./allure-cli/bin/`.
  - ./allure-cli/bin/allure generate target/allure-results  # Gera relatórios do Allure a partir dos resultados dos testes que estão no diretório `target/allure-results`.
  - echo -e $ENVIRONMENT_VAR > environment.properties  # Cria um arquivo chamado `environment.properties` e escreve nele o conteúdo da variável de ambiente `$ENVIRONMENT_VAR`.
  - echo -e $EXECUTOR_VAR > executor.json  # Cria um arquivo chamado `executor.json` e escreve nele o conteúdo da variável de ambiente `$EXECUTOR_VAR`.
  - cp -rf environment.properties target/allure-results  # Copia o arquivo `environment.properties` para o diretório `target/allure-results`.
  - cp -rf executor.json target/allure-results  # Copia o arquivo `executor.json` para o diretório `target/allure-results`.
  - cp -rf allure-report/* target/allure-results  # Copia todos os arquivos e diretórios do diretório `allure-report` para o diretório `target/allure-results`.
  - ./allure-cli/bin/allure generate target/allure-results/ --single-file --clean  # Gera novamente os relatórios do Allure a partir do diretório `target/allure-results`, especificando que queremos gerar um único arquivo de relatório e que queremos limpar os arquivos temporários existentes antes de gerar os novos relatórios.
  - mkdir -p target/allure-report  # Cria o diretório `target/allure-report`. Este é o local onde os novos relatórios gerados serão armazenados.
  - mv -f allure-report/* target/allure-report  # Move todos os arquivos e diretórios do diretório `allure-report` para o diretório `target/allure-report`, substituindo os arquivos existentes, se houver.
```

Esses são os passos executados para preparar e gerar os relatórios do Allure a partir dos resultados dos testes.


### 4. Pages

Este estágio cria uma página web no gitlab pages utilizando Jekyll para exibir os relatórios gerados pelo Allure. Esse estágio funciona
apenas para repositórios públicos. Caso deseje utilizar em repositórios privados, será necessário realizar o deploy utilizando conexão ssh com alguma VM.

```yaml
script:
  - mkdir -p allure-report  # Cria o diretório `allure-report`. Este é o local onde os relatórios do Allure serão armazenados.
  - mv -f target/allure-report/* allure-report  # Move todos os arquivos e diretórios do diretório `target/allure-report` para o diretório `allure-report`, substituindo os arquivos existentes, se houver.
  - cd allure-report  # Navega para o diretório `allure-report`.
  - echo -e 'source "https://rubygems.org"\n\ngem "jekyll"' > Gemfile  # Cria um arquivo `Gemfile` com o conteúdo especificado.
  - gem install bundler  # Instala o Bundler, uma ferramenta para gerenciar as dependências Ruby.
  - bundle install  # Instala as dependências do projeto especificadas no arquivo `Gemfile`.
  - bundle exec jekyll build -d public  # Compila o site Jekyll no diretório `public`.
  - cd ..  # Retorna ao diretório anterior.
  - mv -f allure-report/public ./  # Move todos os arquivos e diretórios do diretório `allure-report/public` para o diretório atual, substituindo os arquivos existentes, se houver.
```

### 5. Resultado final

![allure-report-exemplo.png](allure-cli%2Fallure-report-exemplo.png)