package service;

import static io.restassured.RestAssured.given;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import specs.InitialSpec;

import java.util.Map;

public class BaseService {
    protected RequestSpecification spec;

    public BaseService(){
        spec = InitialSpec.setup();
    }

    protected void setSpec(RequestSpecification spec){
        this.spec = spec;
    }

    protected Response get(String endPoint) {
        return given()
                    .spec(spec)
                .when()
                    .get(endPoint);
    }

    protected Response getQueryParams(String endPoint, Map<String, String> params){
        return given()
                    .spec(spec)
                .when()
                    .queryParams(params)
                    .get(endPoint);
    }

    protected Response getPathParams(String endPoint, Map<String, String> params){
        return given()
                    .spec(spec)
                .when()
                    .pathParams(params)
                    .get(endPoint);
    }


    protected Response post(String endPoint, Object body) {
        return given()
                    .spec(spec)
                    .body(body)
                .when()
                    .post(endPoint);
    }

    protected Response update(String endPoint, Object body, Map<String, String> params) {
        return given()
                    .spec(spec)
                    .pathParams(params)
                    .body(body)
                .when()
                    .put(endPoint);
    }

    protected Response delete(String endPoint, Map<String, String> params) {
        return given()
                    .spec(spec)
                    .pathParams(params)
                .when()
                    .delete(endPoint);

    }


}
