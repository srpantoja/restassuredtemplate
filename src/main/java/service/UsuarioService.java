package service;

import data.dto.usuarios.UsuarioReq;
import io.restassured.response.Response;
import specs.ISpecBuilder;
import specs.AuthSpec;

import java.util.Map;

public class UsuarioService extends BaseService implements ISpecBuilder<UsuarioService> {

    private final String usuario_route = "/usuarios";

    public Response getUsuarios(){
        return get(usuario_route);
    }

    public Response getQueryParams(Map<String, String> params){
        return getQueryParams(usuario_route, params);
    }

    public Response postUsuario(Object usuarioReq){
        return post(usuario_route, usuarioReq);
    }

    @Override
    public UsuarioService withAuth(String token) {
        this.setSpec(AuthSpec.setup(token));
        return this;
    }
}
