package data.providers;

import data.factory.UsuarioFactory;
import data.validacao.UsuarioValidacao;
import org.junit.jupiter.api.Named;
import org.junit.jupiter.params.provider.Arguments;

import java.util.stream.Stream;

public class UsuarioProvider {

    public static Stream<Arguments> retornarBadRequest() {
        return Stream.of(
                Arguments.of(
                        "nome",
                        UsuarioValidacao.NOME_EM_BRANCO,
                        Named.of("com nome em branco", UsuarioFactory.comNomeEmBranco())
                ),
                Arguments.of(
                        "email",
                        UsuarioValidacao.EMAIL_EM_BRANCO,
                        Named.of("com email em branco", UsuarioFactory.comEmailEmBranco())
                ),
                Arguments.of(
                        "password",
                        UsuarioValidacao.SENHA_EM_BRANCO,
                        Named.of("com senha em branco", UsuarioFactory.comSenhaEmBranco())
                ),
                Arguments.of(
                        "administrador",
                        UsuarioValidacao.ADM_EM_BRANCO,
                        Named.of("com adm em branco", UsuarioFactory.comADMEmBranco())
                ),
                Arguments.of(
                        "email",
                        UsuarioValidacao.EMAIL_INVALIDO,
                        Named.of("com email inválido", UsuarioFactory.comEmailInvalido())
                ),
                Arguments.of(
                        "nome",
                        UsuarioValidacao.NOME_OBRIGATORIO,
                        Named.of("sem passar nome", UsuarioFactory.semPassarNome())
                ),
                Arguments.of(
                        "email",
                        UsuarioValidacao.EMAIL_OBRIGATORIO,
                        Named.of("sem passar email", UsuarioFactory.semPassarEmail())
                ),
                Arguments.of(
                        "password",
                        UsuarioValidacao.SENHA_OBRIGATORIO,
                        Named.of("sem passar senha", UsuarioFactory.semPassarSenha())
                ),
                Arguments.of(
                        "administrador",
                        UsuarioValidacao.ADM_OBRIGATORIO,
                        Named.of("sem passar adm", UsuarioFactory.semPassarADM())
                )
        );
    }

}
