package data.dto.usuarios;

public class UsuarioParams {
    public static final String _id = "_id";
    public static final String nome = "nome";
    public static final String email = "email";
    public static final String password = "password";
    public static final String administrador = "administrador";

}
