package data.dto.usuarios;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ListaUsuariosRes {
    private int quantidade;
    private List<UsuarioRes> usuarios;

    public boolean checarSeNomePresenteEmUsuarios(String nome){
        return usuarios.stream().allMatch(usuarioRes -> usuarioRes.getNome().contains(nome));
    }
}
