package data.validacao;

public class UsuarioValidacao {

    public static String POST_SUCESSO = "Cadastro realizado com sucesso";
    public static String NOME_OBRIGATORIO = "nome deve ser uma string";
    public static String EMAIL_OBRIGATORIO = "email deve ser uma string";
    public static String SENHA_OBRIGATORIO = "password deve ser uma string";
    public static String ADM_OBRIGATORIO = "administrador deve ser 'true' ou 'false'";
    public static String NOME_EM_BRANCO = "nome não pode ficar em branco";
    public static String EMAIL_EM_BRANCO = "email não pode ficar em branco";
    public static String SENHA_EM_BRANCO = "password não pode ficar em branco";
    public static String ADM_EM_BRANCO = "administrador deve ser 'true' ou 'false'";
    public static String EMAIL_INVALIDO = "email deve ser um email válido";

}
