package data.factory;

import data.dto.usuarios.UsuarioParams;
import data.dto.usuarios.UsuarioRes;
import data.dto.usuarios.UsuarioReq;
import io.restassured.response.Response;
import org.apache.commons.lang3.StringUtils;
import service.UsuarioService;
import utils.UtilsMap;

import java.util.Map;

public class UsuarioFactory {
    private static final UsuarioService usuarioService = new UsuarioService();

    public static UsuarioReq comTodosOsCamposADM() {
        return UsuarioReq
                .builder()
                    .nome(Factory.nomeValido())
                    .email(Factory.emailValido())
                    .password(Factory.senhaValida())
                    .administrador("true")
                .build();
    }

    public static UsuarioRes jaCadastrado() {
        UsuarioReq usuarioReq = comTodosOsCamposADM();

        Response genericCadRes = usuarioService.postUsuario(usuarioReq);

        return UsuarioRes.builder()
                ._id(genericCadRes.jsonPath().getString("_id"))
                .nome(usuarioReq.getNome())
                .email(usuarioReq.getEmail())
                .password(usuarioReq.getPassword())
                .administrador(usuarioReq.getAdministrador())
                .build();

    }

    public static Map<String, String> comNomeValido(UsuarioRes usuarioRes){
        return UtilsMap.param(UsuarioParams.nome, usuarioRes.getNome());
    }

    public static UsuarioReq comNomeEmBranco(){
        return UsuarioReq
                .builder()
                .nome(StringUtils.EMPTY)
                .email(Factory.emailValido())
                .password(Factory.senhaValida())
                .administrador("true")
                .build();
    }

    public static UsuarioReq comEmailEmBranco(){
        return UsuarioReq
                .builder()
                .nome(Factory.nomeValido())
                .email(StringUtils.EMPTY)
                .password(Factory.senhaValida())
                .administrador("true")
                .build();
    }

    public static UsuarioReq comSenhaEmBranco(){
        return UsuarioReq
                .builder()
                .nome(Factory.nomeValido())
                .email(Factory.emailValido())
                .password(StringUtils.EMPTY)
                .administrador("true")
                .build();
    }

    public static UsuarioReq comADMEmBranco(){
        return UsuarioReq
                .builder()
                .nome(Factory.nomeValido())
                .email(Factory.emailValido())
                .password(Factory.senhaValida())
                .administrador(StringUtils.EMPTY)
                .build();
    }



    public static UsuarioReq comEmailInvalido(){
        return UsuarioReq
                .builder()
                .nome(Factory.nomeValido())
                .email(Factory.emailInvalido())
                .password(Factory.senhaValida())
                .administrador("true")
                .build();
    }

    public static UsuarioReq semPassarNome(){
        return UsuarioReq
                .builder()
                .email(Factory.emailValido())
                .password(Factory.senhaValida())
                .administrador("true")
                .build();
    }
    public static UsuarioReq semPassarEmail(){
        return UsuarioReq
                .builder()
                .nome(Factory.nomeValido())
                .password(Factory.senhaValida())
                .administrador("true")
                .build();
    }


    public static UsuarioReq semPassarSenha(){
        return UsuarioReq
                .builder()
                .nome(Factory.nomeValido())
                .email(Factory.emailValido())
                .administrador("true")
                .build();
    }

    public static UsuarioReq semPassarADM(){
        return UsuarioReq
                .builder()
                .nome(Factory.nomeValido())
                .email(Factory.emailValido())
                .password(Factory.senhaValida())
                .build();
    }



}
