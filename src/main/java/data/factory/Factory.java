package data.factory;

import net.datafaker.Faker;

import java.util.Locale;
import java.util.Random;

public class Factory {
    private static final Faker FAKER = new Faker(new Locale("pt-BR"));
    private static final String SEPARATOR = " ";

    public static String nomeValido(){
        return FAKER.name().firstName() + SEPARATOR +
                FAKER.leagueOfLegends().champion() + SEPARATOR +
                FAKER.name().lastName();
    }

    public static String emailValido(){
        return FAKER.internet().emailAddress();
    }

    public static String emailInvalido(){return FAKER.internet().emailAddress().replace("@", "");}

    public static String senhaValida(){
        return FAKER.internet().password();
    }

}
