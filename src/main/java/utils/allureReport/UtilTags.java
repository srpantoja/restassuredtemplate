package utils.allureReport;

public final class UtilTags {
    public static final String HEALTH = "health";
    public static final String CONTRACT = "contract";
    public static final String SMOKE = "smoke";
    public static final String REGRESSION = "regression";
}
