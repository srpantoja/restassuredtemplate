package utils.allureReport;

public class UtilStory {
    public static final String GET_USUARIO = "GET: buscar todos os usuários cadastrados";
    public static final String POST_USUARIO = "POST: cadastrar usuário";
    public static final String GET_USUARIO_POR_ID = "GET por ID: buscar usuário por ID";
    public static final String DELETE_USUARIO_POR_ID = "DELETE por ID: deletar usuário por ID";
    public static final String PUT_USUARIO_POR_ID = "PUT por ID: atualizar usuário por id";

}
