package tests;

import data.dto.generics.GenericPostRes;
import data.dto.usuarios.ListaUsuariosRes;
import io.restassured.response.Response;
import service.UsuarioService;

public class BaseTest {

    /*
    * Responses personalizadas
    * */
    protected ListaUsuariosRes listaUsuarios;
    protected GenericPostRes genericPostRes;
    protected Response response;

    /*
     * Serviços personalizados
     * */
    protected UsuarioService usuarioService = new UsuarioService();
}
