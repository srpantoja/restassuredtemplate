package tests.usuario;

import data.factory.UsuarioFactory;
import data.dto.usuarios.ListaUsuariosRes;
import data.dto.usuarios.UsuarioRes;
import io.qameta.allure.*;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import tests.BaseTest;
import utils.allureReport.UtilEpic;
import utils.allureReport.UtilFeature;
import utils.allureReport.UtilStory;
import utils.allureReport.UtilTags;

import static org.junit.jupiter.api.Assertions.*;

@Epic(UtilEpic.JORNADA_INICIAL_USUARIO)
@Feature(UtilFeature.GERENCIAMENTO_USUARIO)
public class GetUsuarioTest extends BaseTest {

    @Test
    @Tag(UtilTags.SMOKE)
    @Story(UtilStory.GET_USUARIO)
    @Severity(SeverityLevel.BLOCKER)
    @DisplayName("Teste GET usuario sem parâmetros")
    void testGetUsuarioWithoutParams() {
        listaUsuarios = usuarioService
                            .getUsuarios()
                            .then()
                            .statusCode(HttpStatus.SC_OK)
                            .extract()
                            .as(ListaUsuariosRes.class);
        
        assertEquals(
                listaUsuarios.getQuantidade(),
                listaUsuarios.getUsuarios().size()
        );
    }

    @Test
    @Tag(UtilTags.SMOKE)
    @Story(UtilStory.GET_USUARIO)
    @Severity(SeverityLevel.BLOCKER)
    @DisplayName("Teste Get autor com parâmetros")
    void testGetUsuarioWithParams() {

        UsuarioRes usuarioRes = UsuarioFactory.jaCadastrado();

        response = usuarioService.getQueryParams(UsuarioFactory.comNomeValido(usuarioRes));

        listaUsuarios = response.as(ListaUsuariosRes.class);

        assertAll("assert all",
                () -> assertEquals(
                        listaUsuarios.getQuantidade(),
                        listaUsuarios.getUsuarios().size(),
                        "mensagem de erro"
                ),
                () -> assertTrue(
                        listaUsuarios.checarSeNomePresenteEmUsuarios(usuarioRes.getNome())
                ),
                () -> listaUsuarios.getUsuarios().forEach(usuario ->
                        assertAll(
                                () -> assertFalse(usuario.getAdministrador().isEmpty()),
                                () -> assertFalse(usuario.getPassword().isEmpty()),
                                () -> assertFalse(usuario.getEmail().isEmpty()),
                                () -> assertFalse(usuario.get_id().isEmpty())
                        )
                )
        );
    }
}
