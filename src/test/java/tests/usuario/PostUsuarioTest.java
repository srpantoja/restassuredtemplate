package tests.usuario;

import data.dto.generics.GenericPostRes;
import data.factory.UsuarioFactory;
import data.validacao.UsuarioValidacao;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import tests.BaseTest;
import utils.allureReport.UtilStory;
import utils.allureReport.UtilTags;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.*;

public class PostUsuarioTest extends BaseTest {

    @Test
    @Tag(UtilTags.SMOKE)
    @Story(UtilStory.POST_USUARIO)
    @Severity(SeverityLevel.BLOCKER)
    @DisplayName("Teste Post usuario adm retorna 201 created e mensagem de sucesso")
    void testPostUsuarioADM() {
        genericPostRes = usuarioService
                            .postUsuario(UsuarioFactory.comTodosOsCamposADM())
                            .then()
                            .statusCode(HttpStatus.SC_CREATED)
                            .extract()
                            .as(GenericPostRes.class);

        assertAll(
                () -> assertFalse(genericPostRes.get_id().isEmpty()),
                () -> assertEquals(UsuarioValidacao.POST_SUCESSO, genericPostRes.getMessage())
        );

    }

    @Test
    @Tag(UtilTags.CONTRACT)
    @Story(UtilStory.POST_USUARIO)
    @Severity(SeverityLevel.BLOCKER)
    @DisplayName("Teste validacao json schema post usuarios")
    void testContratoPostUsuarioADM() {
        usuarioService
                .postUsuario(UsuarioFactory.comTodosOsCamposADM())
                .then()
                .body(matchesJsonSchemaInClasspath("schemas/post-usuarios-schemas.json"));
    }

    @ParameterizedTest(name = "{displayName} ({argumentsWithNames})")
    @MethodSource("data.providers.UsuarioProvider#retornarBadRequest")
    @Tag(UtilTags.REGRESSION) @Severity(SeverityLevel.NORMAL)
    @DisplayName("Validar req post /usuarios retorna mensagem de erro")
    void testValidarRetornoBadRequestQuandoPassarDadosInvalidos(String campo, String msgEsperada, Object body) {
        usuarioService
                .postUsuario(body)
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body(campo, equalTo(msgEsperada));
    }

}
